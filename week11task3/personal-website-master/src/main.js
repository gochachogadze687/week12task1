import './styles/style.css';

const emailInput = document.getElementById('email');
const joinForm = document.getElementById('joinForm');
const unsubscribeContainer = document.getElementById('unsubscribeContainer');

function saveEmailToLocalStorage() {
  const emailValue = emailInput.value;
  localStorage.setItem('subscriptionEmail', emailValue);
}

emailInput.addEventListener('input', saveEmailToLocalStorage);

function populateEmailFromLocalStorage() {
  const storedEmail = localStorage.getItem('subscriptionEmail');
  if (storedEmail) {
    emailInput.value = storedEmail;
  }
}

populateEmailFromLocalStorage();

function hideEmailInputAndShowButton() {
  const isValid = emailInput.checkValidity();
  if (isValid) {
    emailInput.style.display = 'none';
    unsubscribeContainer.innerHTML = `
      <button type="button" id="unsubscribeButton">Unsubscribe</button>
    `;
    unsubscribeButton.addEventListener('click', handleUnsubscribe);
  }
}

function handleUnsubscribe() {
  emailInput.style.display = 'block';
  unsubscribeContainer.innerHTML = '';
  localStorage.removeItem('subscriptionEmail');
}

async function handleFormSubmit(e) {
  e.preventDefault();

  try {
    const response = await fetch('http://localhost:3000/subscribe', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ email: emailInput.value }),
    });

    if (response.ok) {
      hideEmailInputAndShowButton();
    } else {
      console.error('Failed to subscribe:', response.statusText);
    }
  } catch (error) {
    console.error('An error occurred:', error);
  }
}



